package com.inditex.zarachallenge.controller.infrastructure.inbound.controller;

import com.inditex.zarachallenge.domain.service.dto.find.ProductDetailResponse;

import com.inditex.zarachallenge.domain.service.dto.find.SimilarProductsResponse;
import com.inditex.zarachallenge.domain.service.ports.input.service.SimilarApplicationService;
import com.inditex.zarachallenge.infrastructure.inbound.controller.SimilarController;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.math.BigDecimal;
import java.util.List;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(SimilarController.class)
public class SimilarControllerTest {

    @Autowired
    MockMvc mockMvc;

    @MockBean
    SimilarApplicationService findSimilarProduct;
    ProductDetailResponse product;
    ProductDetailResponse similar;

    @BeforeEach
    void setUp() {
         product = ProductDetailResponse.builder()
                .id("1")
                .price(BigDecimal.TEN)
                .availability(true)
                .name("product one")
                .build();

         similar = ProductDetailResponse.builder()
                .id("2")
                .price(BigDecimal.TEN)
                .availability(false)
                .name("product two")
                .build();

    }

    @Test
    void whenFindSimilar_ThenReturnsProductWithSimilarProducts() throws Exception {
        // Given
        SimilarProductsResponse expected = SimilarProductsResponse.Builder.newBuilder()
                .productDetail(product)
                .similarProducts(List.of(similar))
                .build();
        when(findSimilarProduct.findSimilarByID(any())).thenReturn(expected);
        mockMvc.perform(get("/product/1/similar").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.product_detail.id").value("1"))
                .andExpect(jsonPath("$.product_detail.name").value("product one"))
                .andExpect(jsonPath("$.product_detail.price").value(10.00))
                .andExpect(jsonPath("$.product_detail.availability").value(true))
                .andExpect(jsonPath("$.similar_products[0].id").value("2"))
                .andExpect(jsonPath("$.similar_products[0].name").value("product two"))
                .andExpect(jsonPath("$.similar_products[0].price").value(10.00))
                .andExpect(jsonPath("$.similar_products[0].availability").value(false));

        verify(findSimilarProduct, times(1)).findSimilarByID(any());
    }
}
