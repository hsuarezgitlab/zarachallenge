package com.inditex.zarachallenge.controller.infrastructure.outbound.dataaccess.product;

import com.inditex.zarachallenge.domain.core.entity.valueobject.OfferId;
import com.inditex.zarachallenge.domain.core.entity.valueobject.ProductId;
import com.inditex.zarachallenge.domain.core.entity.valueobject.SizeId;
import com.inditex.zarachallenge.infrastructure.outbound.dataaccess.offer.entity.OfferEntity;
import com.inditex.zarachallenge.infrastructure.outbound.dataaccess.product.entity.ProductEntity;
import com.inditex.zarachallenge.infrastructure.outbound.dataaccess.size.entity.SizeEntity;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import com.inditex.zarachallenge.infrastructure.outbound.dataaccess.product.repository.ProductJpaRepository;

import java.math.BigDecimal;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.Optional;

@DataJpaTest
public class ProductRepositoryTest {

    @Autowired
    ProductJpaRepository repository;


    @Test
    void findById() {
        // Given
        Optional<ProductEntity> productEntity;

        // When
        productEntity = repository.findById(1);

        // Then
        Assertions.assertTrue(productEntity.isPresent());
        Assertions.assertEquals(1, productEntity.get().getId().intValue());
    }

    @Test
    void Save() {
        // Given
        ProductId productId = new ProductId(1);
        SizeId sizeId = new SizeId(1);
        OfferId offerId = new OfferId(1);
        ZonedDateTime current = ZonedDateTime.now();
        ProductEntity productEntity = ProductEntity.builder()
                .id(productId.getValue())
                .name("product one")
                .offers(List.of(
                        OfferEntity.builder()
                                .id(offerId.getValue())
                                .validFrom(current)
                                .price(BigDecimal.TEN)
                                .build()))
                .sizes(List.of(
                        SizeEntity.builder()
                                .id(sizeId.getValue())
                                .size("M")
                                .availability(true)
                                .lastUpdated(current)
                                .build()
                ))
                .build();

        // When
        ProductEntity saved = repository.save(productEntity);

        // Then
        Assertions.assertEquals(saved.getId(), 1);
        Assertions.assertEquals(saved.getOffers().size(), 1);
        Assertions.assertEquals(saved.getSizes().size(), 1);
    }
}
