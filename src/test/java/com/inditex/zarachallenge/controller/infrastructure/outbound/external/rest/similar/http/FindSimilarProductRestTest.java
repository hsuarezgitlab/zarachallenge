package com.inditex.zarachallenge.controller.infrastructure.outbound.external.rest.similar.http;

import com.inditex.zarachallenge.domain.core.entity.valueobject.ProductId;
import com.inditex.zarachallenge.infrastructure.outbound.external.rest.similars.http.FindSimilarProductsRest;
import com.inditex.zarachallenge.infrastructure.outbound.external.rest.similars.http.exception.ResourceNotFoundHttpClientException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;
import java.util.List;

import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class FindSimilarProductRestTest {
    @Mock
    private RestTemplate restTemplate;

    @InjectMocks
    private FindSimilarProductsRest similarProduct;

    @Test
    void givenProductId_whenGetRequest_thenReturnsSimilarProductIdsArray() {
        // Given
        ProductId productId = new ProductId(1);
        Integer[] expectedIds = new Integer[3];
        expectedIds[0] = 2;
        expectedIds[1] = 3;
        expectedIds[2] = 4;

        when(restTemplate.getForEntity(String.format("http://localhost:3000/product/%s/similarids", productId.getValue()), Integer[].class))
                .thenReturn(new ResponseEntity(expectedIds, HttpStatus.OK));

        // When
        List<Integer> response = similarProduct.findSimilarsById(productId.getValue());

        // Then
        Assertions.assertEquals(expectedIds.length, response.size());
    }

    @Test
    void givenProductId_whenGetRequestReturn404NotFoundException_thenReturnsEmptyList() {
        // Given
        ProductId productId = new ProductId(1);
        when(restTemplate.getForEntity(String.format("http://localhost:3000/product/%s/similarids", productId.getValue()), Integer[].class))
                .thenThrow(new ResourceNotFoundHttpClientException());

        // When
        List<Integer> response = similarProduct.findSimilarsById(productId.getValue());

        // Then
        Assertions.assertEquals(0, response.size());
    }
}
