package com.inditex.zarachallenge.controller.infrastructure.outbound.dataaccess.size;

import com.inditex.zarachallenge.domain.core.entity.valueobject.ProductId;
import com.inditex.zarachallenge.domain.core.entity.valueobject.SizeId;
import com.inditex.zarachallenge.infrastructure.outbound.dataaccess.product.entity.ProductEntity;

import com.inditex.zarachallenge.infrastructure.outbound.dataaccess.size.entity.SizeEntity;
import com.inditex.zarachallenge.infrastructure.outbound.dataaccess.size.repository.SizeJpaRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.time.ZonedDateTime;
import java.util.Optional;

@DataJpaTest
public class SizeRepositoryTest {

    @Autowired
    SizeJpaRepository repository;


    @Test
    void findById() {
        // Given
        Optional<SizeEntity> sizeEntity;

        // When
        sizeEntity = repository.findById(1);

        // Then
        Assertions.assertTrue(sizeEntity.isPresent());
        Assertions.assertEquals(1, sizeEntity.get().getId().intValue());
    }

    @Test
    void Save() {
        // Given
        ProductId productId = new ProductId(1);
        SizeId sizeId = new SizeId(1);
        ZonedDateTime current = ZonedDateTime.now();
        SizeEntity sizeEntity = SizeEntity.builder()
                .id(sizeId.getValue())
                .size("M")
                .availability(true)
                .lastUpdated(current)
                .product(
                        ProductEntity.builder()
                                .id(productId.getValue())
                                .build())
                .build();
        // When
        SizeEntity saved = repository.save(sizeEntity);

        // Then
        Assertions.assertEquals(1, saved.getId());
        Assertions.assertEquals("M", saved.getSize());
    }
}
