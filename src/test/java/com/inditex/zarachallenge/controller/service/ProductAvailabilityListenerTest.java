package com.inditex.zarachallenge.controller.service;

import com.inditex.zarachallenge.domain.service.ProductAvailabilityListener;
import com.inditex.zarachallenge.domain.service.UpdateAvailabilityCommandHandler;
import com.inditex.zarachallenge.domain.service.ports.input.messsage.listener.dto.UpdateAvailabilityCommand;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.annotation.DirtiesContext;

import java.sql.Timestamp;
import java.time.LocalDateTime;

import static org.mockito.Mockito.*;

@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
public class ProductAvailabilityListenerTest {
    @MockBean
    UpdateAvailabilityCommandHandler updateAvailabilityCommandHandler;

    @Autowired
     ProductAvailabilityListener productAvailabilityListener;

    @Test
    void whenReceiptCommand_thenSendCommandToCommandHandler() {
        // Given
        UpdateAvailabilityCommand commandDto = UpdateAvailabilityCommand.builder()
                .sizeId(10L)
                .availability(true)
                .update(Timestamp.valueOf(LocalDateTime.now()))
                .build();

        // When
        productAvailabilityListener.receipt(commandDto);

        // Then
        verify(updateAvailabilityCommandHandler, times(1)).updateAvailability(commandDto);
    }

    @Test
    void whenReceiptCommandIsInvalid_thenRetryUntilTenTimesToSendCommandToCommandHandler() {
        // Given
        UpdateAvailabilityCommand commandDto = UpdateAvailabilityCommand.builder()
                .sizeId(1000L)
                .availability(true)
                .update(Timestamp.valueOf(LocalDateTime.now()))
                .build();

        Mockito.doThrow(new RuntimeException()).when(updateAvailabilityCommandHandler).updateAvailability(any());

        // When
        productAvailabilityListener.receipt(commandDto);

        // Then
        verify(updateAvailabilityCommandHandler, times(5)).updateAvailability(commandDto);
    }

    @Test
    void whenRetryExceedsLimit_thenSendToDeadLetterQueue() {
        // Given
        UpdateAvailabilityCommand commandDto = UpdateAvailabilityCommand.builder()
                .sizeId(1000L)
                .availability(true)
                .update(Timestamp.valueOf(LocalDateTime.now()))
                .build();

        Mockito.doThrow(new RuntimeException()).when(updateAvailabilityCommandHandler).updateAvailability(any());

        // When
        productAvailabilityListener.receipt(commandDto);

        // Then
        verify(updateAvailabilityCommandHandler, times(1)).publishDeadLetterQueue(commandDto);
    }

}
