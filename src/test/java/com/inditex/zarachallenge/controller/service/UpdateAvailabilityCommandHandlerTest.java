package com.inditex.zarachallenge.controller.service;

import static com.inditex.zarachallenge.domain.service.util.FakeDateTime.toTimestamp;
import static org.junit.jupiter.api.Assertions.*;
import com.inditex.zarachallenge.domain.core.entity.Size;
import com.inditex.zarachallenge.domain.core.entity.valueobject.ProductId;
import com.inditex.zarachallenge.domain.core.entity.valueobject.SizeId;
import com.inditex.zarachallenge.domain.core.exception.SizeNotFoundException;
import com.inditex.zarachallenge.domain.service.UpdateAvailabilityCommandHandler;
import com.inditex.zarachallenge.domain.service.ports.input.messsage.listener.dto.UpdateAvailabilityCommand;
import com.inditex.zarachallenge.domain.service.ports.output.repository.SizeRepository;
import com.inditex.zarachallenge.domain.service.util.FakeDateTime;
import org.junit.jupiter.api.Test;
import org.mockito.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.Optional;

import static org.mockito.Mockito.*;

@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
public class UpdateAvailabilityCommandHandlerTest {
    @MockBean
    SizeRepository repository;

    @Autowired
    UpdateAvailabilityCommandHandler updateAvailabilityCommandHandler;

    @Test
    void whenReceiptValidSizeId_thenUpdateSize() {
        // Given
        UpdateAvailabilityCommand commandDto = UpdateAvailabilityCommand.builder()
                .sizeId(10L)
                .availability(true)
                .update(toTimestamp(new FakeDateTime().now()))
                .build();

        SizeId sizeId = new SizeId(commandDto.getSizeId().intValue());

        Size size = Size.Builder
                .builder()
                .size("M")
                .availability(false)
                .lastUpdated(new FakeDateTime().now().minusDays(30L))
                .productId(new ProductId(1))
                .build();

        ArgumentCaptor<Size> captor = ArgumentCaptor.forClass(Size.class);
        when(repository.findById(sizeId)).thenReturn(Optional.of(size));

        // When
        updateAvailabilityCommandHandler.updateAvailability(commandDto);
        verify(repository).save(captor.capture());

        // Then
        Size sizeCaptorValue = captor.getValue();
        assertTrue(sizeCaptorValue.isAvailability());
        assertEquals(sizeCaptorValue.getLastUpdated(), new FakeDateTime().now());
    }

    @Test
    void whenReceiptInvalidSizeId_thenThrowSizeNotFoundException() {
        // Given
        UpdateAvailabilityCommand commandDto = UpdateAvailabilityCommand.builder()
                .sizeId(10L)
                .availability(true)
                .update(toTimestamp(new FakeDateTime().now()))
                .build();

        SizeId sizeId = new SizeId(commandDto.getSizeId().intValue());
        when(repository.findById(sizeId)).thenReturn(Optional.empty());

        // Then
        assertThrows(SizeNotFoundException.class, () -> updateAvailabilityCommandHandler.updateAvailability(commandDto));
    }
}
