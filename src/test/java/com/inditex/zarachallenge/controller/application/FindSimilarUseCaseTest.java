package com.inditex.zarachallenge.controller.application;

import com.inditex.zarachallenge.domain.core.entity.Offer;
import com.inditex.zarachallenge.domain.core.entity.Product;
import com.inditex.zarachallenge.domain.core.entity.Size;
import com.inditex.zarachallenge.domain.core.entity.valueobject.*;
import com.inditex.zarachallenge.domain.core.exception.ProductNotFoundException;
import com.inditex.zarachallenge.domain.service.dto.find.SimilarProductsQuery;
import com.inditex.zarachallenge.domain.service.dto.find.SimilarProductsResponse;
import com.inditex.zarachallenge.domain.service.ports.input.service.SimilarApplicationService;
import com.inditex.zarachallenge.domain.service.ports.output.repository.ProductRepository;
import com.inditex.zarachallenge.domain.service.ports.output.service.FindSimilarProduct;
import com.inditex.zarachallenge.domain.service.util.FakeDateTime;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.annotation.DirtiesContext;

import static org.junit.jupiter.api.Assertions.*;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

import static org.mockito.Mockito.*;

@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
public class FindSimilarUseCaseTest {
    @MockBean
    ProductRepository productRepository;
    @MockBean
    FindSimilarProduct similarProducts;
    @Autowired
    SimilarApplicationService findService;
    Product product;
    Product similarProduct;

    @BeforeEach
    void setUp() {
        product = Product.Builder.builder()
                .id(new ProductId(1))
                .name(new ProductName("Fake product"))
                .offers(List.of(Offer.Builder.builder()
                        .id(new OfferId(1))
                        .productId(new ProductId(1))
                        .validFrom(new FakeDateTime().now())
                        .price(new ProductPrice(BigDecimal.TEN))
                        .build()))
                .sizes(List.of(Size.Builder.builder()
                        .sizeId(new SizeId(1))
                        .productId(new ProductId(1))
                        .availability(true)
                        .lastUpdated(new FakeDateTime().now())
                        .size("M")
                        .build()))
                .build();

        similarProduct = Product.Builder.builder()
                .id(new ProductId(2))
                .name(new ProductName("Similar product"))
                .offers(List.of(Offer.Builder.builder()
                        .id(new OfferId(2))
                        .productId(new ProductId(2))
                        .validFrom(new FakeDateTime().now())
                        .price(new ProductPrice(BigDecimal.TEN))
                        .build()))
                .sizes(List.of(Size.Builder.builder()
                        .sizeId(new SizeId(2))
                        .productId(new ProductId(2))
                        .availability(true)
                        .lastUpdated(new FakeDateTime().now())
                        .size("M")
                        .build()))
                .build();
    }


    @Test
    void givenExistingProductId_whenFindById_thenReturnsSimilarProductsResponse() {
        // Given
        ProductId givenProductId = new ProductId(1);
        SimilarProductsQuery givenQueryDto =
                SimilarProductsQuery.builder().id(String.valueOf(givenProductId.getValue())).build();
        Product savedProduct = product;
        when(similarProducts.findSimilarsById(givenProductId.getValue())).thenReturn(List.of(2));
        when(productRepository.findById(givenProductId)).thenReturn(Optional.of(savedProduct));
        when(productRepository.findById(new ProductId(2))).thenReturn(Optional.of(similarProduct));

        // When
        SimilarProductsResponse response = findService.findSimilarByID(givenQueryDto);

        // Then
        assertEquals(response.getProductDetail().getId(), "1");
        assertEquals(response.getSimilarProducts().get(0).getId(), "2");
    }

    @Test
    void givenNotExistingProductId_whenFindById_thenThrowsProductNotFoundException() {
        // Given
        ProductId givenProductId = new ProductId(1);
        SimilarProductsQuery givenQueryDto =
                SimilarProductsQuery.builder().id(String.valueOf(givenProductId.getValue())).build();
        Product savedProduct = product;
        when(similarProducts.findSimilarsById(givenProductId.getValue())).thenReturn(List.of(2));
        when(productRepository.findById(givenProductId)).thenReturn(Optional.empty());

        // When Then
        assertThrows(ProductNotFoundException.class, () -> findService.findSimilarByID(givenQueryDto));
    }

}
