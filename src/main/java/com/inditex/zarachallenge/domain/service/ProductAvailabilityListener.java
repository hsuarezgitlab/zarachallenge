package com.inditex.zarachallenge.domain.service;

import com.inditex.zarachallenge.domain.service.ports.input.messsage.listener.ProductRequestMessageListener;
import com.inditex.zarachallenge.domain.service.ports.input.messsage.listener.dto.UpdateAvailabilityCommand;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Slf4j
@Component
@RequiredArgsConstructor
public class ProductAvailabilityListener implements ProductRequestMessageListener {
    private final UpdateAvailabilityCommandHandler updateAvailabilityCommandHandler;
    @Override
    public void receipt(UpdateAvailabilityCommand commandDto) {
        updateAvailabilityCommandHandler.updateAvailability(commandDto);
    }
    @Override
    public void sendToDeadLetterQueue(RuntimeException e, UpdateAvailabilityCommand commandDto){
        updateAvailabilityCommandHandler.publishDeadLetterQueue(commandDto);
    }
}
