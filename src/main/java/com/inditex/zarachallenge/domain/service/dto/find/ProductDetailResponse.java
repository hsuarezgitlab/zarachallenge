package com.inditex.zarachallenge.domain.service.dto.find;

import lombok.Builder;
import lombok.Getter;

import java.math.BigDecimal;

@Getter
@Builder
public class ProductDetailResponse {
    private final String id;
    private final String name;
    private final BigDecimal price;
    private final boolean availability;

}
