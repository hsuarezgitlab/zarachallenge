package com.inditex.zarachallenge.domain.service;

import com.inditex.zarachallenge.domain.service.ports.input.messsage.listener.dto.UpdateAvailabilityCommand;
import org.springframework.stereotype.Component;

@Component
public interface UpdateAvailabilityCommandHandler {
    void updateAvailability(UpdateAvailabilityCommand updateAvailabilityCommand);
    void publishDeadLetterQueue(UpdateAvailabilityCommand updateAvailabilityCommand);
}
