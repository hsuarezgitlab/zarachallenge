package com.inditex.zarachallenge.domain.service.ports.output.repository;

import com.inditex.zarachallenge.domain.core.entity.Offer;
import com.inditex.zarachallenge.domain.core.entity.valueobject.OfferId;
import com.inditex.zarachallenge.domain.core.entity.valueobject.ProductId;

import java.util.List;
import java.util.Optional;

public interface OfferRepository extends Repository<Offer, OfferId> {
    Optional<List<Offer>> findById(ProductId productId);
}
