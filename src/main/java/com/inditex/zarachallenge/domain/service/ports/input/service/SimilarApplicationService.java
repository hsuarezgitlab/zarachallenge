package com.inditex.zarachallenge.domain.service.ports.input.service;

import com.inditex.zarachallenge.domain.service.dto.find.SimilarProductsQuery;
import com.inditex.zarachallenge.domain.service.dto.find.SimilarProductsResponse;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

public interface SimilarApplicationService {
    SimilarProductsResponse findSimilarByID(SimilarProductsQuery similarProductsQuery);
}
