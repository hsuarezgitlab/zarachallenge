package com.inditex.zarachallenge.domain.service.ports.output.repository;

import com.inditex.zarachallenge.domain.core.entity.Size;
import com.inditex.zarachallenge.domain.core.entity.valueobject.ProductId;
import com.inditex.zarachallenge.domain.core.entity.valueobject.SizeId;

import java.util.Optional;
import java.util.Set;

public interface SizeRepository extends Repository<Size, SizeId> {
    Optional<Size> findById(SizeId sizeId);
    void save(Size size);
}
