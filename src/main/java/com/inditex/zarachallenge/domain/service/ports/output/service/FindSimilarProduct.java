package com.inditex.zarachallenge.domain.service.ports.output.service;

import java.util.List;

public interface FindSimilarProduct {
    List<Integer> findSimilarsById(Integer productId);
}
