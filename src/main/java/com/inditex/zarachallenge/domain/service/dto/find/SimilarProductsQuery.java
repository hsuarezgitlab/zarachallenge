package com.inditex.zarachallenge.domain.service.dto.find;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import shaded_package.javax.validation.constraints.NotNull;

@Getter
@Builder
@AllArgsConstructor
public class SimilarProductsQuery {
    @NotNull
    private final String id;
}
