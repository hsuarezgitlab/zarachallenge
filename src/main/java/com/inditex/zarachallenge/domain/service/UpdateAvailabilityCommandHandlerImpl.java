package com.inditex.zarachallenge.domain.service;

import com.inditex.zarachallenge.domain.core.entity.Size;
import com.inditex.zarachallenge.domain.core.entity.valueobject.SizeId;
import com.inditex.zarachallenge.domain.core.exception.SizeNotFoundException;
import com.inditex.zarachallenge.domain.service.ports.input.messsage.listener.dto.UpdateAvailabilityCommand;
import com.inditex.zarachallenge.domain.service.ports.output.repository.SizeRepository;
import com.inditex.zarachallenge.domain.service.util.FakeDateTime;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import java.util.Optional;

@Slf4j
@Component
@RequiredArgsConstructor
public class UpdateAvailabilityCommandHandlerImpl implements UpdateAvailabilityCommandHandler {
    private final SizeRepository sizeRepository;

    @Override
    public void updateAvailability(UpdateAvailabilityCommand updateAvailabilityCommand) {
        Size sizeSaved = findSizeByID(updateAvailabilityCommand.getSizeId());
        Size sizeToSave = Size.Builder
                .builder()
                .sizeId(sizeSaved.getId())
                .productId(sizeSaved.getProductId())
                .size(sizeSaved.getSize())
                .availability(updateAvailabilityCommand.isAvailability())
                .lastUpdated(FakeDateTime.parse(updateAvailabilityCommand.getUpdate()))
                .build();

        sizeRepository.save(sizeToSave);
    }

    private Size findSizeByID(Long id) {
        SizeId sizeId = new SizeId(id.intValue());
        Optional<Size> OptionalSize = sizeRepository.findById(sizeId);
        return OptionalSize.orElseThrow(() -> new SizeNotFoundException(String.format("size with id: %s not found", sizeId.getValue())));
    }

    @Override
    public void publishDeadLetterQueue(UpdateAvailabilityCommand updateAvailabilityCommand) {
        log.error(String.format("sending to dead letter queue update message for size_id: %s", updateAvailabilityCommand.getSizeId()));
    }
}
