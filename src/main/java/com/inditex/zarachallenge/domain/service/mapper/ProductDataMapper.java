package com.inditex.zarachallenge.domain.service.mapper;

import com.inditex.zarachallenge.domain.service.dto.find.ProductDetailResponse;
import com.inditex.zarachallenge.domain.service.dto.find.SimilarProductsResponse;
import com.inditex.zarachallenge.domain.core.entity.Product;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

@Component
public class ProductDataMapper {
    public SimilarProductsResponse productsToSimilarProductsResponse(Product product, List<Product> similarProducts) {
        Function<Product, ProductDetailResponse> productToProductDetail = (p) ->
            ProductDetailResponse.builder()
                    .id(p.getId().getValue().toString())
                    .name(p.getName().getValue())
                    .price(p.getPrice())
                    .availability(p.isAvailable())
                    .build();

        return SimilarProductsResponse.Builder
                .newBuilder()
                .productDetail(productToProductDetail.apply(product))
                .similarProducts(similarProducts.stream().map(productToProductDetail).collect(Collectors.toList()))
                .build();
    }
}
