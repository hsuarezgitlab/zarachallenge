package com.inditex.zarachallenge.domain.service.ports.output.repository;

import java.util.Optional;

public interface Repository <T, ID>{
    Optional<T> findById(ID id);
}
