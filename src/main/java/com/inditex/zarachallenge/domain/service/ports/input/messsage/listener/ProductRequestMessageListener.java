package com.inditex.zarachallenge.domain.service.ports.input.messsage.listener;

import com.inditex.zarachallenge.domain.service.ports.input.messsage.listener.dto.UpdateAvailabilityCommand;
import org.springframework.retry.annotation.Backoff;
import org.springframework.retry.annotation.Recover;
import org.springframework.retry.annotation.Retryable;

public interface ProductRequestMessageListener {
    @Retryable(retryFor = RuntimeException.class, maxAttempts = 5, backoff = @Backoff(delay = 500))
    void receipt(UpdateAvailabilityCommand commandDto);
    @Recover
    void sendToDeadLetterQueue(RuntimeException e, UpdateAvailabilityCommand commandDto);
}
