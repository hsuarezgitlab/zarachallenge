package com.inditex.zarachallenge.domain.service.util;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.sql.Timestamp;
import java.time.ZonedDateTime;

@Slf4j
@Component
public class FakeDateTime {
    public static String currentDate;

    @Autowired
    public FakeDateTime(@Value("${date}") String currentDate) {
        FakeDateTime.currentDate = currentDate;
    }

    public FakeDateTime() {

    }
    public ZonedDateTime now() {
        return ZonedDateTime.parse(currentDate);
    }

    public static ZonedDateTime parse(Timestamp timestamp) {
        String timeString = timestamp.toString().replace(" ", "T").concat("Z");
        return ZonedDateTime.parse(timeString);
    }

    public static Timestamp toTimestamp(ZonedDateTime zonedDateTime) {
        String timeString = zonedDateTime.toString().replace("T", " ").substring(0,23);
        return Timestamp.valueOf(timeString);
    }
}
