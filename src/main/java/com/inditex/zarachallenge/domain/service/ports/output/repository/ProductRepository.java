package com.inditex.zarachallenge.domain.service.ports.output.repository;

import com.inditex.zarachallenge.domain.core.entity.Product;
import com.inditex.zarachallenge.domain.core.entity.valueobject.ProductId;

import java.util.Optional;

public interface ProductRepository extends Repository<Product, ProductId>{
    Optional<Product> findById(ProductId id);
}
