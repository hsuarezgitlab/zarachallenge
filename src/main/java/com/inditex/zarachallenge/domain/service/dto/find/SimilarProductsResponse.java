package com.inditex.zarachallenge.domain.service.dto.find;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class SimilarProductsResponse {
    @JsonProperty("product_detail")
    private final ProductDetailResponse productDetail;
    @JsonProperty("similar_products")

    private final List<ProductDetailResponse> similarProducts;

    private SimilarProductsResponse(Builder builder) {
        productDetail = builder.productDetail;
        similarProducts = builder.similarProducts;
    }

    public ProductDetailResponse getProductDetail() {
        return productDetail;
    }

    public List<ProductDetailResponse> getSimilarProducts() {
        return similarProducts;
    }


    public static final class Builder {
        private ProductDetailResponse productDetail;
        private List<ProductDetailResponse> similarProducts;

        private Builder() {
        }

        public static Builder newBuilder() {
            return new Builder();
        }

        public Builder productDetail(ProductDetailResponse val) {
            productDetail = val;
            return this;
        }

        public Builder similarProducts(List<ProductDetailResponse> val) {
            similarProducts = val;
            return this;
        }

        public SimilarProductsResponse build() {
            return new SimilarProductsResponse(this);
        }
    }
}
