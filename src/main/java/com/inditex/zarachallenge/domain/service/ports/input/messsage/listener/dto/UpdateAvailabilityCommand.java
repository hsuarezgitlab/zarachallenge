package com.inditex.zarachallenge.domain.service.ports.input.messsage.listener.dto;

import lombok.Builder;
import lombok.Getter;

import java.sql.Timestamp;

@Getter
@Builder
public class UpdateAvailabilityCommand {

    private Long sizeId;

    private boolean availability;

    private Timestamp update;
}
