package com.inditex.zarachallenge.domain.core.entity;

import com.inditex.zarachallenge.domain.core.entity.valueobject.SizeId;
import com.inditex.zarachallenge.domain.core.entity.valueobject.ProductId;
import lombok.Getter;

import java.time.ZonedDateTime;

public class Size extends BaseEntity<SizeId>{
    private ProductId productId;
    private final String size;
    private final boolean  availability;
    private final ZonedDateTime lastUpdated;

    private Size(Builder builder) {
        super.setId(builder.sizeId);
        productId = builder.productId;
        size = builder.size;
        availability = builder.availability;
        lastUpdated = builder.lastUpdated;
    }

    public ProductId getProductId() {
        return productId;
    }

    public String getSize() {
        return size;
    }

    public boolean isAvailability() {
        return availability;
    }

    public ZonedDateTime getLastUpdated() {
        return lastUpdated;
    }

    public static final class Builder {
        private SizeId sizeId;
        private ProductId productId;
        private String size;
        private boolean availability;
        private ZonedDateTime lastUpdated;

        private Builder() {
        }

        public static Builder builder() {
            return new Builder();
        }

        public Builder sizeId(SizeId val) {
            sizeId = val;
            return this;
        }

        public Builder productId(ProductId val) {
            productId = val;
            return this;
        }

        public Builder size(String val) {
            size = val;
            return this;
        }

        public Builder availability(boolean val) {
            availability = val;
            return this;
        }

        public Builder lastUpdated(ZonedDateTime val) {
            lastUpdated = val;
            return this;
        }

        public Size build() {
            return new Size(this);
        }
    }
}
