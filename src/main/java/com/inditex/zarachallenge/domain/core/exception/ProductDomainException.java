package com.inditex.zarachallenge.domain.core.exception;

public class ProductDomainException extends DomainException{
    public ProductDomainException(String message) {
        super(message);
    }

    public ProductDomainException(String message, Throwable cause) {
        super(message, cause);
    }
}
