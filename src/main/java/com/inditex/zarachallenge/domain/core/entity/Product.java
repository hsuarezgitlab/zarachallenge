package com.inditex.zarachallenge.domain.core.entity;

import com.inditex.zarachallenge.domain.core.entity.valueobject.ProductId;
import com.inditex.zarachallenge.domain.core.entity.valueobject.ProductName;
import com.inditex.zarachallenge.domain.service.util.FakeDateTime;

import java.math.BigDecimal;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

public class Product extends AggregateRoot<ProductId> {
    private final ProductName name;
    private final List<Size> sizes;
    private final List<Offer> offers;

    private Product(Builder builder) {
        super.setId(builder.productId);
        name = builder.name;
        sizes = builder.sizes;
        offers = builder.offers;
    }

    public boolean isAvailable() {
        return sizes.stream().anyMatch(Size::isAvailability);
    }
/*
  Comparator<Persona> comparadorMultiple= Comparator.comparing(Persona::getNombre).thenComparing(Comparator.comparing(Persona::getApellidos)).thenComparing(Comparator.comparing(Persona::getEdad));

 */
    public BigDecimal getPrice() {
        Optional<Offer> validOffer = offers.stream()
                .filter(offer -> offer.getValidFrom().isBefore(new FakeDateTime().now()))
                .max(Comparator.comparing(Offer::getValidFrom));

        return validOffer.isPresent() ? validOffer.get().getPrice().getValue() : BigDecimal.ZERO;

    }

    public static final class Builder {
        private ProductId productId;
        private ProductName name;
        private List<Size> sizes;
        private List<Offer> offers;

        private Builder() {
        }

        public static Builder builder() {
            return new Builder();
        }

        public Builder id(ProductId val) {
            productId = val;
            return this;
        }

        public Builder name(ProductName val) {
            name = val;
            return this;
        }

        public Builder sizes(List<Size> val) {
            sizes = val;
            return this;
        }

        public Builder offers(List<Offer> val) {
            offers = val;
            return this;
        }

        public Product build() {
            return new Product(this);
        }
    }

    public ProductName getName() {
        return name;
    }

    public List<Size> getSizes() {
        return sizes;
    }

    public List<Offer> getOffers() {
        return offers;
    }
}
