package com.inditex.zarachallenge.domain.core.entity;

import com.inditex.zarachallenge.domain.core.entity.valueobject.OfferId;
import com.inditex.zarachallenge.domain.core.entity.valueobject.ProductPrice;
import com.inditex.zarachallenge.domain.core.entity.valueobject.ProductId;
import lombok.Getter;
import java.time.ZonedDateTime;


public class Offer extends BaseEntity<OfferId> {
    private ProductId productId;
    private final ZonedDateTime validFrom;
    private final ProductPrice price;

    private Offer(Builder builder) {
        super.setId(builder.offerId);
        productId = builder.productId;
        validFrom = builder.validFrom;
        price = builder.price;
    }

    public static final class Builder {
        private OfferId offerId;
        private ProductId productId;
        private ZonedDateTime validFrom;
        private ProductPrice price;

        private Builder() {
        }

        public static Builder builder() {
            return new Builder();
        }

        public Builder id(OfferId val) {
            offerId = val;
            return this;
        }

        public Builder productId(ProductId val) {
            productId = val;
            return this;
        }

        public Builder validFrom(ZonedDateTime val) {
            validFrom = val;
            return this;
        }

        public Builder price(ProductPrice val) {
            price = val;
            return this;
        }

        public Offer build() {
            return new Offer(this);
        }
    }

    public ProductId getProductId() {
        return productId;
    }

    public ZonedDateTime getValidFrom() {
        return validFrom;
    }

    public ProductPrice getPrice() {
        return price;
    }
}
