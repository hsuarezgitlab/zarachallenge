package com.inditex.zarachallenge.domain.core.entity.valueobject;

public class ProductName extends BaseObjectValue<String>{
    public ProductName(String value) {
        super(value);
    }
}
