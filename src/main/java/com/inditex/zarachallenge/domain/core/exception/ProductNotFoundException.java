package com.inditex.zarachallenge.domain.core.exception;

public class ProductNotFoundException extends DomainException{
    public ProductNotFoundException(String message) {
        super(message);
    }

    public ProductNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }
}
