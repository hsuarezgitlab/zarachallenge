package com.inditex.zarachallenge.domain.core.entity.valueobject;

public class SizeId extends BaseObjectValue<Integer> {
    public SizeId(Integer value) {
        super(value);
    }
}
