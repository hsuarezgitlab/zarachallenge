package com.inditex.zarachallenge.domain.core.entity.valueobject;

import java.util.Objects;

public abstract class BaseObjectValue<T> {
    private final T value;

    protected BaseObjectValue(T value) {
        this.value = value;
    }

    public T getValue() {
        return value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BaseObjectValue<?> baseObjectValue = (BaseObjectValue<?>) o;
        return Objects.equals(value, baseObjectValue.value);
    }

    @Override
    public int hashCode() {
        return Objects.hash(value);
    }
}
