package com.inditex.zarachallenge.domain.core.exception;

public class SizeNotFoundException extends DomainException{
    public SizeNotFoundException(String message) {
        super(message);
    }

    public SizeNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }
}
