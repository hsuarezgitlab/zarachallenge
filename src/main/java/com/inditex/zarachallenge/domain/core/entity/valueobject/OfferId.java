package com.inditex.zarachallenge.domain.core.entity.valueobject;

public class OfferId extends BaseObjectValue<Integer> {
    public OfferId(Integer value) {
        super(value);
    }
}
