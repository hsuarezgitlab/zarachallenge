package com.inditex.zarachallenge.domain.core.entity.valueobject;

public class ProductId extends BaseObjectValue<Integer> {
    public ProductId(Integer value) {
        super(value);
    }
}
