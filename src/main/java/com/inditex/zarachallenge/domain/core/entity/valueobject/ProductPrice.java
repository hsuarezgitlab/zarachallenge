package com.inditex.zarachallenge.domain.core.entity.valueobject;

import java.math.BigDecimal;

public class ProductPrice extends BaseObjectValue<BigDecimal> {
    public ProductPrice(BigDecimal value) {
        super(value);
    }
}
