package com.inditex.zarachallenge.application.find;

import com.inditex.zarachallenge.domain.core.entity.Product;
import com.inditex.zarachallenge.domain.core.exception.ProductNotFoundException;
import com.inditex.zarachallenge.domain.service.dto.find.SimilarProductsQuery;
import com.inditex.zarachallenge.domain.service.dto.find.SimilarProductsResponse;
import com.inditex.zarachallenge.domain.service.mapper.ProductDataMapper;
import com.inditex.zarachallenge.domain.service.ports.input.service.SimilarApplicationService;
import com.inditex.zarachallenge.domain.service.ports.output.repository.ProductRepository;
import com.inditex.zarachallenge.domain.core.entity.valueobject.ProductId;
import com.inditex.zarachallenge.domain.service.ports.output.service.FindSimilarProduct;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import shaded_package.javax.validation.Valid;

import java.util.List;
import java.util.Optional;

@Service
@Slf4j
@RequiredArgsConstructor
public class FindSimilarProducts implements SimilarApplicationService {
    private final ProductDataMapper mapper;
    private final ProductRepository productRepository;
    private final FindSimilarProduct similarProducts;

    @Override
    public SimilarProductsResponse findSimilarByID(@Valid SimilarProductsQuery similarProductsQuery) {
        ProductId productId = new ProductId(Integer.parseInt(similarProductsQuery.getId()));
        Product product = productRepository
                .findById(productId)
                .orElseThrow(()-> new ProductNotFoundException(String.format("product with id: %s not found",  productId.getValue())));

        List<ProductId> similarProductsIds = similarProducts
                .findSimilarsById(productId.getValue())
                .stream()
                .map(ProductId::new)
                .toList();

        List<Product> similarList =  similarProductsIds.stream()
                .map(productRepository::findById)
                .filter(Optional::isPresent)
                .map(Optional::get)
                .toList();
        return mapper.productsToSimilarProductsResponse(product, similarList);
    }
}
