package com.inditex.zarachallenge.infrastructure.outbound.external.rest.similars.http.exception;

import org.springframework.http.HttpStatusCode;

public class UnexpectedHttpClientException extends RuntimeException{
    public UnexpectedHttpClientException(HttpStatusCode httpStatusCode) {
        super(String.format("unexpected http client error with status_code: %s ", httpStatusCode));
    }
}
