package com.inditex.zarachallenge.infrastructure.outbound.dataaccess.product.adapter;

import com.inditex.zarachallenge.domain.service.ports.output.repository.ProductRepository;
import com.inditex.zarachallenge.domain.core.entity.Product;
import com.inditex.zarachallenge.domain.core.entity.valueobject.ProductId;
import com.inditex.zarachallenge.infrastructure.outbound.dataaccess.product.mapper.ProductDataAccessMapper;
import com.inditex.zarachallenge.infrastructure.outbound.dataaccess.product.repository.ProductJpaRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
@RequiredArgsConstructor
public class ProductRepositoryJpaAdapter implements ProductRepository {
    private final ProductJpaRepository productJpaRepository;
    private final ProductDataAccessMapper productDataAccessMapper;

    @Override
    public Optional<Product> findById(ProductId productId) {
        return productJpaRepository.findById(productId.getValue()).map(productDataAccessMapper::productEntityToProduct);
    }
}
