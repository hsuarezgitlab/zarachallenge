package com.inditex.zarachallenge.infrastructure.outbound.messaging.kafka.mapper;

import com.inditex.zarachallenge.domain.service.ports.input.messsage.listener.dto.UpdateAvailabilityCommand;
import com.inditex.zarachallenge.infrastructure.outbound.messaging.kafka.model.ProductAvailabilityEvent;
import org.springframework.stereotype.Component;

@Component
public class UpdateAvailabilityMapper {
    public UpdateAvailabilityCommand messageToDto (ProductAvailabilityEvent productAvailabilityEvent) {
        return UpdateAvailabilityCommand.builder()
                .sizeId(productAvailabilityEvent.getSizeId())
                .availability(productAvailabilityEvent.isAvailability())
                .update(productAvailabilityEvent.getUpdate())
                .build();
    }
}
