package com.inditex.zarachallenge.infrastructure.outbound.dataaccess.size.repository;

import com.inditex.zarachallenge.infrastructure.outbound.dataaccess.size.entity.SizeEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface SizeJpaRepository extends JpaRepository<SizeEntity, Integer> {
    Optional<SizeEntity> findById(Integer id);
}
