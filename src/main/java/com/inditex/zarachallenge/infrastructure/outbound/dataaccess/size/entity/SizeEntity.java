package com.inditex.zarachallenge.infrastructure.outbound.dataaccess.size.entity;

import com.inditex.zarachallenge.infrastructure.outbound.dataaccess.product.entity.ProductEntity;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.ZonedDateTime;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "SIZE")
@Entity
public class SizeEntity {
    @Id
    @Column(name = "size_id")
    private Integer id;
    private String size;
    private boolean availability;
    private ZonedDateTime lastUpdated;
    @ManyToOne
    @JoinColumn(name = "PRODUCT_ID")
    private ProductEntity product;
}
