package com.inditex.zarachallenge.infrastructure.outbound.dataaccess.offer.mapper;

import com.inditex.zarachallenge.domain.core.entity.Offer;
import com.inditex.zarachallenge.domain.core.entity.Product;
import com.inditex.zarachallenge.domain.core.entity.valueobject.OfferId;
import com.inditex.zarachallenge.domain.core.entity.valueobject.ProductId;
import com.inditex.zarachallenge.domain.core.entity.valueobject.ProductName;
import com.inditex.zarachallenge.domain.core.entity.valueobject.ProductPrice;
import com.inditex.zarachallenge.infrastructure.outbound.dataaccess.offer.entity.OfferEntity;
import com.inditex.zarachallenge.infrastructure.outbound.dataaccess.product.entity.ProductEntity;
import com.inditex.zarachallenge.infrastructure.outbound.dataaccess.size.mapper.SizeDataAccessMapper;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class OfferDataAccessMapper {
    public List<Offer> offersEntitiesToOffers(List<OfferEntity> offerEntities) {
        return offerEntities.stream().map(this::offerEntityToOffer).collect(Collectors.toList());
    }

    private Offer offerEntityToOffer(OfferEntity offerEntity) {
        return Offer.Builder.builder()
                .id(new OfferId(offerEntity.getId()))
                .validFrom(offerEntity.getValidFrom())
                .price(new ProductPrice(offerEntity.getPrice()))
                .productId(new ProductId(offerEntity.getId()))
                .build();
    }
}
