package com.inditex.zarachallenge.infrastructure.outbound.external.rest.similars.http;

import com.inditex.zarachallenge.domain.service.ports.output.service.FindSimilarProduct;
import com.inditex.zarachallenge.infrastructure.outbound.external.rest.similars.http.exception.HttpClientExceptionHandler;
import com.inditex.zarachallenge.infrastructure.outbound.external.rest.similars.http.exception.ResourceNotFoundHttpClientException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.Collections;
import java.util.List;
import java.util.Objects;

@Slf4j
@Component
public class FindSimilarProductsRest implements FindSimilarProduct {
    private String URL = "http://localhost:3000/product/%s/similarids";

    @Autowired
    private RestTemplate restTemplate;
    @Override
    public List<Integer> findSimilarsById(Integer productId) {
        restTemplate.setErrorHandler(new HttpClientExceptionHandler());
        try {
            ResponseEntity<Integer[]> responseEntity =
                    restTemplate.getForEntity(String.format(URL, productId), Integer[].class);
            List<Integer> result = List.of(Objects.requireNonNull(responseEntity.getBody()));
            return result;
        } catch (ResourceNotFoundHttpClientException exception) {
            log.error(String.format("resource not found exception on http client to product-id: %s", productId.toString()));
        }
        return Collections.emptyList();
    }
}
