package com.inditex.zarachallenge.infrastructure.outbound.dataaccess.product.mapper;

import com.inditex.zarachallenge.domain.core.entity.Product;
import com.inditex.zarachallenge.domain.core.entity.valueobject.*;
import com.inditex.zarachallenge.infrastructure.outbound.dataaccess.offer.mapper.OfferDataAccessMapper;
import com.inditex.zarachallenge.infrastructure.outbound.dataaccess.product.entity.ProductEntity;
import com.inditex.zarachallenge.infrastructure.outbound.dataaccess.size.mapper.SizeDataAccessMapper;
import org.springframework.stereotype.Component;


@Component
public class ProductDataAccessMapper {

    public Product productEntityToProduct(ProductEntity productEntity) {
        return Product.Builder
                .builder()
                .id(new ProductId(productEntity.getId()))
                .name(new ProductName(productEntity.getName()))
                .sizes(new SizeDataAccessMapper().sizeEntitiesToSizes(productEntity.getSizes()))
                .offers(new OfferDataAccessMapper().offersEntitiesToOffers(productEntity.getOffers()))
                .build();
    }
}
