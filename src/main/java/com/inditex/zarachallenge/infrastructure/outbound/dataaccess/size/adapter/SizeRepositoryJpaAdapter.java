package com.inditex.zarachallenge.infrastructure.outbound.dataaccess.size.adapter;

import com.inditex.zarachallenge.domain.core.entity.Size;
import com.inditex.zarachallenge.domain.core.entity.valueobject.SizeId;
import com.inditex.zarachallenge.domain.service.ports.output.repository.SizeRepository;
import com.inditex.zarachallenge.infrastructure.outbound.dataaccess.size.mapper.SizeDataAccessMapper;
import com.inditex.zarachallenge.infrastructure.outbound.dataaccess.size.repository.SizeJpaRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
@RequiredArgsConstructor
public class SizeRepositoryJpaAdapter implements SizeRepository {
    private final SizeJpaRepository sizeJpaRepository;
    private final SizeDataAccessMapper sizeDataAccessMapper;

    @Override
    public Optional<Size> findById(SizeId SizeId) {
        return sizeJpaRepository.findById(SizeId.getValue()).map(sizeDataAccessMapper::sizeEntityToSize);
    }

    @Override
    public void save(Size size) {
        sizeJpaRepository.save(sizeDataAccessMapper.sizeToSizeEntity(size));
    }
}
