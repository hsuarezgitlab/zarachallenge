package com.inditex.zarachallenge.infrastructure.outbound.dataaccess.size.mapper;

import com.inditex.zarachallenge.domain.core.entity.Size;
import com.inditex.zarachallenge.domain.core.entity.valueobject.*;
import com.inditex.zarachallenge.infrastructure.outbound.dataaccess.product.entity.ProductEntity;
import com.inditex.zarachallenge.infrastructure.outbound.dataaccess.size.entity.SizeEntity;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class SizeDataAccessMapper {
    public List<Size> sizeEntitiesToSizes(List<SizeEntity> sizeEntities) {
        return sizeEntities.stream().map(this::sizeEntityToSize).collect(Collectors.toList());
    }

    public Size sizeEntityToSize(SizeEntity sizeEntity) {
        return Size.Builder.builder()
                .sizeId(new SizeId(sizeEntity.getId()))
                .size(sizeEntity.getSize())
                .availability(sizeEntity.isAvailability())
                .lastUpdated(sizeEntity.getLastUpdated())
                .productId(new ProductId(sizeEntity.getProduct().getId()))
                .build();
    }

    public SizeEntity sizeToSizeEntity(Size size) {
        return SizeEntity.builder()
                .id(size.getId().getValue())
                .availability(size.isAvailability())
                .size(size.getSize())
                .lastUpdated(size.getLastUpdated())
                .product(ProductEntity.builder().id(size.getProductId().getValue()).build())
                .build();
    }
}
