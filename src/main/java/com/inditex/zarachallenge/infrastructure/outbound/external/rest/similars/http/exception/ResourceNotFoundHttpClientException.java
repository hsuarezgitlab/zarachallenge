package com.inditex.zarachallenge.infrastructure.outbound.external.rest.similars.http.exception;

public class ResourceNotFoundHttpClientException extends RuntimeException{
    public ResourceNotFoundHttpClientException(String message) {
        super(message);
    }

    public ResourceNotFoundHttpClientException(String message, Throwable cause) {
        super(message, cause);
    }

    public ResourceNotFoundHttpClientException() {

    }

}
