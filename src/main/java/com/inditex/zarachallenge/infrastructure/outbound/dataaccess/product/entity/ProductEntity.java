package com.inditex.zarachallenge.infrastructure.outbound.dataaccess.product.entity;


import com.inditex.zarachallenge.infrastructure.outbound.dataaccess.offer.entity.OfferEntity;
import com.inditex.zarachallenge.infrastructure.outbound.dataaccess.size.entity.SizeEntity;
import jakarta.persistence.*;
import lombok.*;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "PRODUCT")
@Entity
public class ProductEntity {
    @Id
    private Integer id;
    private String name;
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "product", cascade = CascadeType.ALL)
    private List<SizeEntity> sizes;
    @OneToMany(mappedBy = "product", cascade = CascadeType.ALL)
    private List<OfferEntity> offers;
}
