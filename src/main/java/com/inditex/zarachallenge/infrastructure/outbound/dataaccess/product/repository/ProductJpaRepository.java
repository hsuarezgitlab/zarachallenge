package com.inditex.zarachallenge.infrastructure.outbound.dataaccess.product.repository;

import com.inditex.zarachallenge.infrastructure.outbound.dataaccess.product.entity.ProductEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ProductJpaRepository extends JpaRepository<ProductEntity, Integer> {
    Optional<ProductEntity> findById(Integer id);
}
