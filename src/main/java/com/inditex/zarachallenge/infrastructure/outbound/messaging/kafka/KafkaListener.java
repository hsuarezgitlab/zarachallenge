package com.inditex.zarachallenge.infrastructure.outbound.messaging.kafka;

import java.util.function.Consumer;

import com.inditex.zarachallenge.domain.service.ports.input.messsage.listener.ProductRequestMessageListener;
import com.inditex.zarachallenge.infrastructure.outbound.messaging.kafka.model.ProductAvailabilityEvent;
import com.inditex.zarachallenge.infrastructure.outbound.messaging.kafka.mapper.UpdateAvailabilityMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.messaging.Message;
import org.springframework.stereotype.Component;

@Slf4j
@Component
@RequiredArgsConstructor
public class KafkaListener {
	private final ProductRequestMessageListener productRequestMessageListener;
	private final UpdateAvailabilityMapper mapper;
	@Bean
	public Consumer<Message<ProductAvailabilityEvent>> KafkaConsumer() {
		return message -> {
			log.info(String.format("update availability event receipt for size_id : %s", message.getPayload().getSizeId()));
			productRequestMessageListener.receipt(mapper.messageToDto(message.getPayload()));
		};
	}

}
