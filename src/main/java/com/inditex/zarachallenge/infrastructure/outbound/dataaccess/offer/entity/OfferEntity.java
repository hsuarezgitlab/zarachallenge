package com.inditex.zarachallenge.infrastructure.outbound.dataaccess.offer.entity;

import com.inditex.zarachallenge.infrastructure.outbound.dataaccess.product.entity.ProductEntity;
import jakarta.persistence.*;
import lombok.*;

import java.math.BigDecimal;
import java.time.ZonedDateTime;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "OFFER")
@Entity
@EqualsAndHashCode
public class OfferEntity {
    @Id
    private Integer id;
    private ZonedDateTime validFrom;
    private BigDecimal price;
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "PRODUCT_ID")
    private ProductEntity product;
}
