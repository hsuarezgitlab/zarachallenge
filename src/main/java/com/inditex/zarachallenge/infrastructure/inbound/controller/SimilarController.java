package com.inditex.zarachallenge.infrastructure.inbound.controller;

import com.inditex.zarachallenge.domain.service.dto.find.SimilarProductsQuery;
import com.inditex.zarachallenge.domain.service.dto.find.SimilarProductsResponse;
import com.inditex.zarachallenge.domain.service.ports.input.service.SimilarApplicationService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping(path = "/product")
public class SimilarController {

    private final SimilarApplicationService similarService;
	@RequestMapping(method = RequestMethod.GET ,path = "/{id}/similar", produces = {MediaType.APPLICATION_JSON_VALUE}, consumes = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<SimilarProductsResponse> findSimilarProducts (@PathVariable String id) {
        SimilarProductsResponse response = similarService.findSimilarByID(
                SimilarProductsQuery
                        .builder()
                        .id(id)
                        .build());
        return ResponseEntity.ok(response);
    }
}
